![ez logo](/resources/images/ez/ez-smiley-small-logo.png)
# Kubernetes
This project prepare the framework for the Kubernetes cluster of the by adding namespaces and ingress configuration.

It includes:

- namespaces creation: see https://gitlab.com/sela-1090/students/ez/infrastructures/kubernetes/-/blob/main/ez-framework/templates/namespaces.yaml
- ingress configuration (TBD)

The project creates HELM chart, and upload it to DockerHub. See https://hub.docker.com/r/ezezeasy/ez-framework

The project also contains a GitLab-CI piprline that build versions of the HELM chart and upload it to DockerHub. see https://gitlab.com/sela-1090/students/ez/infrastructures/kubernetes/-/blob/main/.gitlab-ci.yml

